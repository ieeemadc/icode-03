package com.jsinterpreter.rhinotest01;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class PracticeEditorActivity extends AppCompatActivity implements CompilerFragment.SendMessage{

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int level;
    private View decorView;
    private Menu menu_toolbar;

    //fragmento result
    private ResultFragment resultFragment=new ResultFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_editor);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTitle("Rhino Interpreter");
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        toolbar.setTitle("Practice ");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onKeyDown(int key, KeyEvent keycode){
        super.onKeyDown(key, keycode);

        if(key==KeyEvent.KEYCODE_BACK){
            exit();
            return true;
        }else {
            return true;
        }

    }

    public int getLevel(){
        return level;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        CompilerFragment co = new CompilerFragment();
        Bundle datos = new Bundle();
        co.setLevel(level);

        adapter.addFragment(co,getResources().getString(R.string.title_tab_code));
        adapter.addFragment(resultFragment,getResources().getString(R.string.title_tab_result));
        viewPager.setAdapter(adapter);
    }

    private void exit(){

        AlertDialog.Builder Dialogoadvertencia = new AlertDialog.Builder(this);
        Dialogoadvertencia.setTitle("Are you sure want to exit?");
        Dialogoadvertencia.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PracticeEditorActivity.this.finish();
            }
        });
        Dialogoadvertencia.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog menuDrop = Dialogoadvertencia.create();
        menuDrop.show();
    }

    @Override
    public void sendData(String msg) {
        resultFragment.getData(msg);
    }

    @Override
    public void sendError(int error) {
        resultFragment.getError(error);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);

        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return mFragmentTitleList.get(position);
        }
    }

    /* Action Bar Settings */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_tab, menu);
        menu_toolbar= menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_settings:

                break;
            case android.R.id.home:
                exit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
