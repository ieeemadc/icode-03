package com.jsinterpreter.rhinotest01;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;
/**
 * Explain the Exercise or the Basics in lessons
 * Each lessons contains different explain
 */

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExplainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExplainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    MenuItem menuVisible;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText txtResult;
    private int level;
    private int page;
    public ExplainFragment() {
        // Required empty public constructor
    }


    public void setLevel(int l){
        level=l;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExplainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExplainFragment newInstance(String param1, String param2) {
        ExplainFragment fragment = new ExplainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explain, container, false);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_compile);
        WebView wView = (WebView) view.findViewById(R.id.webView);
        wView.getSettings().setJavaScriptEnabled(true);
        level = getActivity().getIntent().getIntExtra("level", 1);
        page = getActivity().getIntent().getIntExtra("page", 1);
        wView.loadUrl(ManagerLevel.getInstance().getExplain(level,page));
        //wView.loadDataWithBaseURL("file:///android_asset/", str, "text/html", "utf-8", null);
        wView.setWebViewClient(new MyWebViewClient());

        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
                viewPager.setCurrentItem( viewPager.getCurrentItem()+1 );
            }
        });

        return view;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main_tab, menu);
        menu.clear();
        menuVisible = menu.findItem(R.id.action_delete);
    }

    @Override
    public void setUserVisibleHint(boolean isVisible){
        super.setUserVisibleHint(isVisible);
        if(menuVisible!=null){
            menuVisible.setVisible(!isVisible);

        }
    }





}
