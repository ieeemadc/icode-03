package com.jsinterpreter.rhinotest01;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.EcmaError;
import org.mozilla.javascript.EvaluatorException;
import org.mozilla.javascript.JavaScriptException;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.WrappedException;

/**
 * Created by Home on 5/11/2015.
 */



public class Interprete {

    Context cx;
    String resultOK;
    public Interprete() {
        init();
    }

    private void init(){
        cx = Context.enter();
        cx.setOptimizationLevel(-1);
        resultOK = "Undefined";
    }

    public String compilar(String codigo,Object objeto,String nombre){
        Object resultado = 0;
        cx = Context.enter();
        try {
            Scriptable scope = cx.initStandardObjects();
            Object jsOut = Context.javaToJS(objeto, scope);
            ScriptableObject.putProperty(scope, nombre, jsOut);
            resultado = cx.evaluateString(scope, codigo, "Line ", 1, null);
            System.err.println(Context.toString(resultado));
        }

        catch (EcmaError ee) {
            System.err.println(ee.getMessage());
            resultado = ee.getMessage();
            ee.printStackTrace();
        }

        catch (WrappedException we) {
            // Some form of exception was caught by JavaScript and
            // propagated up.
            resultado = we.getMessage();
            System.err.println(we.getWrappedException().toString());
            we.printStackTrace();

        }

        catch (EvaluatorException ee) {
            // Some form of JavaScript error.
            resultado=ee.getMessage();
            System.err.println("js: " + ee.getMessage());
        }

        catch (JavaScriptException jse) {
            // Some form of JavaScript error.
            resultado=jse.getMessage();
            System.err.println("js: " + jse.getMessage());
        }
        /*
        catch (IOException ioe) {
            System.err.println(ioe.toString());

        }
        */
        finally {

            Context.exit();
            if (resultado.toString().contains(this.resultOK)) {
                return "Ok";
            }
            else {

                return resultado.toString();
            }

        }
    }
}
