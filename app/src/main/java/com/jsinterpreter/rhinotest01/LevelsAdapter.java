package com.jsinterpreter.rhinotest01;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Jonathan on 8/5/2016.
 */
public class LevelsAdapter extends RecyclerView.Adapter<LevelsAdapter.LevelViewHolder> {

    private ArrayList<Level> mDataSet;
    private Context context;

    public LevelsAdapter(ArrayList<Level> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public void onBindViewHolder(LevelsAdapter.LevelViewHolder holder, final int position) {
        holder.title_entry.setText(mDataSet.get(position).getName());
        holder.description_entry.setText(mDataSet.get(position).getDescription());
        holder.icon_entry.setText(""+position);
        holder.level=mDataSet.get(position).getLevelNumber();
        if(position>0){
            holder.imgLock.setVisibility(mDataSet.get(position-1).getSuccess() == true ? View.INVISIBLE : View.VISIBLE);
        }else{
            holder.imgLock.setVisibility(mDataSet.get(position).getSuccess() == true ? View.INVISIBLE : View.VISIBLE);
        }
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position==0){
                    Intent gotoLevel = new Intent(context,IntroductionActivity.class);
                    context.startActivity(gotoLevel);
                }else{
                    if(mDataSet.get(position-1).getSuccess()){
                        Intent gotoLevel = new Intent(context,ContentLevelActivity.class);
                        gotoLevel.putExtra("level", position-1);
                        ((Activity)(context)).startActivityForResult(gotoLevel,1);
                    }else{
                        Toast.makeText(context,"Blocked",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public LevelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.level_row_layout, parent, false);
        context = parent.getContext();
        LevelViewHolder levelViewHolder = new LevelViewHolder(v,context);
        return levelViewHolder;
    }


    public static class LevelViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView title_entry, description_entry, icon_entry;
        ImageView imgLock;
        Context context;
        int level=1;
        LevelViewHolder(View itemView,Context c) {
            super(itemView);
            context = c;
            cardView = (CardView) itemView.findViewById(R.id.user_layout);
            title_entry = (TextView) itemView.findViewById(R.id.title_entry);
            description_entry = (TextView) itemView.findViewById(R.id.description_entry);
            icon_entry = (TextView) itemView.findViewById(R.id.icon_entry);
            imgLock = (ImageView) itemView.findViewById(R.id.imglock);
        }
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
