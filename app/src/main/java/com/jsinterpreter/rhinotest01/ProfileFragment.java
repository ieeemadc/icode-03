package com.jsinterpreter.rhinotest01;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.github.lzyzsd.circleprogress.ArcProgress;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArcProgress progress;
    private IconRoundCornerProgressBar progress2;
    private TextView lblCurrentLevel,lblToNextLevel,lblRanking,lblLessonsCurrent,lblLessonsProgress;
    private Button btnFreeEditor;
    private int progress_xp_current;
    private int max_xp;
    private int progress_lessons_current;
    private int max_lessons;
    private int delay_time;
    View view;
    //private RangeSliderView progressBarLevels;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        progress = (ArcProgress)view.findViewById(R.id.arc_progress);
        progress2 =  (IconRoundCornerProgressBar)view.findViewById(R.id.progress_round);
        lblCurrentLevel = (TextView)view.findViewById(R.id.lblLevelCurrent);
        lblToNextLevel = (TextView)view.findViewById(R.id.lblcontent1);
        lblRanking = (TextView)view.findViewById(R.id.lblRanking);
        lblLessonsCurrent = (TextView)view.findViewById(R.id.lblLessonsCurrent);
        lblLessonsProgress = (TextView)view.findViewById(R.id.lblLessonProgress);
        btnFreeEditor = (Button)view.findViewById(R.id.btnFreeEditor);
        //progressBarLevels = (RangeSliderView)view.findViewById(R.id.progressBarLevel);
        btnFreeEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoPractice = new Intent(getContext(),PracticeEditorActivity.class);
                gotoPractice.putExtra("training", true);
                getContext().startActivity(gotoPractice);
            }
        });
        //initialize

        progress2.setMax(100);
        progress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animate();
            }
        });
        progress2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animate2();
            }
        });
        return view;
    }
    @Override
    public void onResume(){
        super.onResume();
        max_xp = ManagerLevel.getInstance().getProgressLimit();
        progress_xp_current = ManagerLevel.getInstance().getCurrentXP();
        progress.setMax(max_xp);
        lblCurrentLevel.setText("Level : "+(ManagerLevel.getInstance().getLevelXP()+1));
        lblRanking.setText("Ranking : "+ManagerLevel.getInstance().getSkill());
        lblLessonsCurrent.setText("Lessons complete : "+ManagerLevel.getInstance().LessonsSucess());
        if(progress_xp_current==105){
            lblToNextLevel.setText("All lessons complete!");
        }else{
            lblToNextLevel.setText("To next level : "+ (max_xp-progress_xp_current));
        }

        progress_lessons_current = ManagerLevel.getInstance().LessonProgress();
        progress_lessons_current = (progress_lessons_current*100)/48;//to know the percentage
        lblLessonsProgress.setText("Percentage of complete lessons : "+progress_lessons_current+"%");
    }

    @Override
    public void setUserVisibleHint(boolean isVisible){
        super.setUserVisibleHint(isVisible);
        if(progress!=null){
            progress.setProgress(0);
            progress2.setProgress(0);
        }
        if(isVisible){
            animate();
            animate2();
        }
    }

    private void animate2(){
        ProgressBarAnimation anim = new ProgressBarAnimation(progress2,0,progress_lessons_current);
        anim.setDuration(1000);
        progress2.startAnimation(anim);
    }
    private void animate(){

        ProgressArcAnimation anim = new ProgressArcAnimation(progress, 0, progress_xp_current);
        if(progress_xp_current<=30){
            delay_time=650;
        }else if(progress_xp_current>30 && progress_xp_current<=60){
            delay_time=900;
        }else if(progress_xp_current>60){
            delay_time=1200;
        }
        anim.setDuration(delay_time);
        progress.startAnimation(anim);
        //Toast.makeText(getContext(),"Progress "+progressBarLevels.getRangeCount(),Toast.LENGTH_SHORT).show();
    }

    public class ProgressArcAnimation extends Animation {
        private ArcProgress progressBar;
        private IconRoundCornerProgressBar progressBar2;
        private float from;
        private float  to;
        public ProgressArcAnimation(ArcProgress progressArc, float from, float to) {
            super();
            this.progressBar = progressArc;
            this.from = from;
            this.to = to;
        }
        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            progressBar.setProgress((int) value);
        }
    }


    public class ProgressBarAnimation extends Animation {
        private IconRoundCornerProgressBar progressBar2;
        private float from;
        private float  to;
        public ProgressBarAnimation(IconRoundCornerProgressBar progressBar, float from, float to) {
            super();
            this.progressBar2 = progressBar;
            this.from = from;
            this.to = to;
        }
        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            progressBar2.setProgress((int) value);
        }
    }


}
