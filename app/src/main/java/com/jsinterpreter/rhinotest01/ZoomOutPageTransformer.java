package com.jsinterpreter.rhinotest01;

import android.animation.ObjectAnimator;
import android.os.Build;
import android.support.v4.animation.AnimatorListenerCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

/**
 * Created by Home on 29/11/2015.
 */
public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
    private static final float MIN_SCALE = 0.25f;
    private static final float SCALE = 0.03f;

    @Override
    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();
        //Log.d("Position ", " "+position);
        if (position < 0) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            //min 0.25 max 0.90
            //x -> ? % si se 0.90 es el 100%
            //view.setScaleX(1f);
            //view.setScaleY(1f);
            ObjectAnimator anim = ObjectAnimator.ofFloat(view,"translationZ",0,0);
            anim.start();
        //}else if(position < 0){

        } else if (position <= 1) {

            float scaleFactor = Math.abs(MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position)));
            scaleFactor = scaleFactor - MIN_SCALE;//to min
            float percent = (scaleFactor*100f)/(0.75f);
            percent/=100.0f;
            //Log.d("Scale ", " "+scaleFactor + " percent "+ percent+" scale new "+ (1.0f+(SCALE*percent)));
            //view.setScaleX(1.0f+(SCALE*percent));
            //view.setScaleY(1.0f+(SCALE*percent));
            ObjectAnimator anim = ObjectAnimator.ofFloat(view,"translationZ",0,38);
            anim.start();

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.

            //view.setScaleX(1f);
            //view.setScaleY(1f);

            ObjectAnimator anim = ObjectAnimator.ofFloat(view,"translationZ",0,0);
            anim.start();

        }
    }
}
