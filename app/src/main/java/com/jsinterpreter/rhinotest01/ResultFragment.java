package com.jsinterpreter.rhinotest01;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.io.Serializable;

public class ResultFragment extends Fragment {
    myData miDato;
    EditText txtResult;
    View view;
    private int level;
    private int page;
    private int hasError=1;
    private boolean isTraining=false;
    private CoordinatorLayout main_layout;
    public ResultFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_result, container, false);
        // Inflate the layout for this fragment
        txtResult = (EditText)view.findViewById(R.id.txtResult);
        Funciones.getInstancia().setContexto(view.getContext());
        Funciones.getInstancia().inicializarDialog();
        Funciones.getInstancia().setEditText(txtResult);
        level = getActivity().getIntent().getIntExtra("level", 1);
        page = getActivity().getIntent().getIntExtra("page", 1);
        main_layout = (CoordinatorLayout) view.findViewById(R.id.container);
        isTraining = getActivity().getIntent().getBooleanExtra("training", false);
        LinearLayout layoutOk = (LinearLayout)view.findViewById(R.id.linearLayoutOK);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_result);
        if(isTraining){
            layoutOk.setVisibility(View.INVISIBLE);
            fab.setEnabled(false);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluateCode();
            }
        });
        return view;
    }

    public void getData(String msg){
        if(msg!=null && txtResult!=null){
            if(!msg.equals("Ok")){
                txtResult.setText("");
                txtResult.setText(msg);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle bund){
        super.onSaveInstanceState(bund);
        bund.putSerializable("view",miDato);
    }
    private void evaluateCode(){
        if(hasError!=0){
            ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
            viewPager.setCurrentItem(0);
        }else{
            //save data
            ManagerLevel.getInstance().setPageSuccess(level,page,true);
            getActivity().finish();
        }
    }
    public void getError(int error){
        hasError=error;
        if(view!=null){
            switch (hasError){
                case 0:
                    Snackbar.make(view.findViewById(R.id.container), "Executed correctly", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case 1:
                    Snackbar.make(view.findViewById(R.id.container), "The code contains errors", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case 2:
                    Snackbar.make(view.findViewById(R.id.container), "The code does not contain the requirements", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
            }
        }
    }

    class myData implements Serializable{
        private View view;
        public View getView() {
            return view;
        }
        public void setView(View view) {
            this.view = view;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.action_delete){
            txtResult.setText("");
        }
        return super.onOptionsItemSelected(item);
    }

}
