package com.jsinterpreter.rhinotest01;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.ArcProgress;

public class SuccessActivity extends AppCompatActivity {
    private Button btnContinue;
    private int xp_saved;
    private ArcProgress progressCount;
    private TextView lblComplete,lblLevel,lblSkill;
    private int level;
    private int max_xp;
    private int level_xp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        btnContinue = (Button)findViewById(R.id.btnContinue);
        progressCount = (ArcProgress)findViewById(R.id.arc_progress);
        lblComplete = (TextView)findViewById(R.id.lblComplete);
        lblLevel = (TextView)findViewById(R.id.lblLevelNumber);
        lblSkill = (TextView)findViewById(R.id.lblSkill);
        level = getIntent().getIntExtra("level", 1);
        lblComplete.setText("Lesson : "+(level+1)+", Complete!");
        level_xp = (ManagerLevel.getInstance().getLevelXP()+1);
        lblLevel.setText("Level : "+level_xp);
        lblSkill.setText(ManagerLevel.getInstance().getSkill());
        max_xp = ManagerLevel.getInstance().getProgressLimit();
        progressCount.setMax(max_xp);
        xp_saved = ManagerLevel.getInstance().getCurrentXP();
        progressCount.setProgress(xp_saved);
        progressCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countXP();
            }
        });
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ManagerLevel.getInstance().addXP();
                SuccessActivity.this.finish();
            }
        });
        ManagerLevel.getInstance().addXP();
    }

    private void pause(int miliseconds){
        try {
            Thread.sleep(miliseconds);
        } catch(InterruptedException e) {}
    }

    private void countXP(){
        MyAsyncTask incrementProgres = new MyAsyncTask();
        incrementProgres.execute();
    }


    private void animate(){
        ProgressArcAnimation anim = new ProgressArcAnimation(progressCount, xp_saved, xp_saved+15);
        anim.setDuration(800);
        progressCount.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if((xp_saved+15)==max_xp){
                    Toast.makeText(SuccessActivity.this,"Level : "+level_xp+" complete!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public class ProgressArcAnimation extends Animation {
        private ArcProgress progressBar;
        private float from;
        private float  to;
        public ProgressArcAnimation(ArcProgress progressArc, float from, float to) {
            super();
            this.progressBar = progressArc;
            this.from = from;
            this.to = to;
        }
        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            progressBar.setProgress((int) value);
        }
    }

    private class MyAsyncTask extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            pause(700);
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //nothig
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result){
                //Toast.makeText(SuccessActivity.this, "Tarea finalizada",
                //        Toast.LENGTH_SHORT).show();
                animate();

            }

        }
        @Override
        protected void onCancelled() {
            //Toast.makeText(SuccessActivity.this, "Tarea cancelada!",
            //        Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        countXP();
    }
}
