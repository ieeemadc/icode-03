package com.jsinterpreter.rhinotest01;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class QuizActivity extends AppCompatActivity {

    int selected_position=-1;
    CheckBox chk1, chk2,chk3;
    ArrayList<CheckBox> lstCheques;
    TextView lblQuestion,lblCode;
    int level;
    int question=0;
    int answer;
    boolean revisado=false;
    FloatingActionButton fab;
    ColorStateList defaultColor;
    int errors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        chk1 = (CheckBox)findViewById(R.id.check1);
        chk2 = (CheckBox)findViewById(R.id.check2);
        chk3 = (CheckBox)findViewById(R.id.check3);
        lblQuestion = (TextView)findViewById(R.id.lblQuestion);
        lblCode = (TextView)findViewById(R.id.lblCode);
        level = getIntent().getIntExtra("level", 1);
        errors = getIntent().getIntExtra("errors", 0);
        question = getIntent().getIntExtra("question", 1);
        setTitle("Question : "+question);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String content = ManagerLevel.getInstance().getQuestion(level,question-1);
        if(content.contains("? :")){
            int p = content.indexOf("? :")+3;
            int l = content.length();
            String q = content.substring(0,p);
            String c = content.substring(p,l);
            lblQuestion.setText(q);
            lblCode.setText(c);
            lblCode.setVisibility(View.VISIBLE);
        }else{
            lblQuestion.setText(content);
        }

        chk1.setText(ManagerLevel.getInstance().getAnswer(level,question-1,0));
        chk2.setText(ManagerLevel.getInstance().getAnswer(level,question-1,1));
        chk3.setText(ManagerLevel.getInstance().getAnswer(level,question-1,2));
        answer = (Integer.parseInt(ManagerLevel.getInstance().getAnswer(level,question-1,3))-1);

        lstCheques = new ArrayList<>();
        lstCheques.add(chk1);
        lstCheques.add(chk2);
        lstCheques.add(chk3);
        checkListener(chk1);
        checkListener(chk2);
        checkListener(chk3);

        fab = (FloatingActionButton) findViewById(R.id.fab_quiz);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(revisado==false){
                    check_answer();
                    revisado=true;
                }else{
                    level = getIntent().getIntExtra("level", 1);
                    question = getIntent().getIntExtra("question",1);
                    if(question==2){//limit of questions
                        if(errors<2){//limit of errors aviables
                            ManagerLevel.getInstance().setPageSuccess(level,3,true);
                            if(ManagerLevel.getInstance().getSucces(level)==false){//si no ha ganado xp
                                ManagerLevel.getInstance().setSuccess(level,true);
                                Intent gotoSuccess = new Intent(QuizActivity.this,SuccessActivity.class);
                                gotoSuccess.putExtra("level",level);
                                startActivity(gotoSuccess);
                                QuizActivity.this.finish();
                            }
                        }
                        QuizActivity.this.finish();
                    }else{
                        Intent gotoNext = new Intent(QuizActivity.this,QuizActivity.class);
                        gotoNext.putExtra("level",level);
                        gotoNext.putExtra("question",question+1);
                        gotoNext.putExtra("errors",errors);
                        startActivity(gotoNext);
                        QuizActivity.this.finish();
                    }
                }
            }
        });
        defaultColor = fab.getBackgroundTintList();
        fab.setEnabled(false);
        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#E0E0E0")));

    }

    private void check_answer(){

        Log.d("Level ","Answer "+answer);
        Log.d("Level ","Position Answer "+selected_position);

        if(answer==selected_position){
            Toast.makeText(this,"Correct answer",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"Incorrect answer",Toast.LENGTH_SHORT).show();
            errors++;
        }
        chk1.setEnabled(false);
        chk2.setEnabled(false);
        chk3.setEnabled(false);

    }


    private void exit(){

        AlertDialog.Builder Dialogoadvertencia = new AlertDialog.Builder(this);
        Dialogoadvertencia.setTitle("Are you sure want to exit?");
        Dialogoadvertencia.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                QuizActivity.this.finish();
            }
        });
        Dialogoadvertencia.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog menuDrop = Dialogoadvertencia.create();
        menuDrop.show();
    }


    private void checkListener(CheckBox chk){
        chk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view) {
                if (((CheckBox) view).isChecked())
                {
                    for (int i = 0; i < lstCheques.size(); i++) {
                        if (lstCheques.get(i) == view)
                            selected_position = i;
                        else
                            lstCheques.get(i).setChecked(false);
                    }
                    fab.setEnabled(true);
                    fab.setBackgroundTintList(defaultColor);
                }
                else
                {
                    selected_position=-1;
                    fab.setEnabled(false);
                    fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#E0E0E0")));
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int key, KeyEvent keycode){
        super.onKeyDown(key, keycode);
        if(key==KeyEvent.KEYCODE_BACK){
            exit();
            return true;
        }else {
            return true;
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_settings:

                break;
            case android.R.id.home:
                exit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
