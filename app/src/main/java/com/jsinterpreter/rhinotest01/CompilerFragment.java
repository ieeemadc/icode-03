package com.jsinterpreter.rhinotest01;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.AndroidException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
/**
 *
 * Content the editor and the functionality of this using a instance of interprete
 * results are send to the next fragment (result fragment) with flags
 * zero = no error
 * one = error
 * two = forget any statement of the exercise
 */

public class CompilerFragment extends Fragment {

    SendMessage sm;
    EditText txtCodigo;
    Interprete interprete;
    int level;
    int page;
    int hasError=0;
    boolean isTraining=false;
    public CompilerFragment() {
        // Required empty public constructor
    }
    public void setLevel(int l){
        level=l;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_compiler, container, false);

        level  = getActivity().getIntent().getIntExtra("level", 1);
        page = getActivity().getIntent().getIntExtra("page", 1);
        isTraining = getActivity().getIntent().getBooleanExtra("training", false);
        txtCodigo = (EditText)view.findViewById(R.id.txtCodigo);
        if(isTraining==false){
            txtCodigo.setText(ManagerLevel.getInstance().getCode(level,page));
        }else{
            txtCodigo.setText("");
        }

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_compile);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                execute();
                ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
                viewPager.setCurrentItem( viewPager.getCurrentItem()+1 );
            }
        });

        //añadiendo interprete
        interprete = new Interprete();
        return view;
    }
    interface SendMessage{
        public void sendData(String msg);
        public void sendError(int error);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            sm = (SendMessage)activity;
        }catch (ClassCastException e){
            throw  new ClassCastException("you need to implement send data method");
        }

    }
    private void execute(){
        sm.sendData("");
        String result = interprete.compilar(txtCodigo.getText().toString(), Funciones.getInstancia(), "console");
        if(result.contains("Line #")){
            sm.sendData(result);
            hasError=1;
        } else {
            if(isTraining==false){
                if(ManagerLevel.getInstance().containsCode(txtCodigo.getText().toString(),level)){
                    hasError=0;
                }else{
                    hasError=2;
                }
            }else{
                hasError=0;
            }
        }
        sm.sendError(hasError);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.action_delete){
            txtCodigo.setText("");
        }
        return super.onOptionsItemSelected(item);
    }


}
