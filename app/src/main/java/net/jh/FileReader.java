package net.jh;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Jheovany on 31/05/2016.
 */
public class FileReader {

    public static String readFromAssets(Context context, String filename) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(filename)));

        // do reading, usually loop until end of file reading
        StringBuilder sb = new StringBuilder();
        String mLine = reader.readLine();
        while (mLine != null) {
            sb.append(mLine); // process line
            sb.append('\n');
            mLine = reader.readLine();
        }

        reader.close();

        return sb.toString();
    }
}
