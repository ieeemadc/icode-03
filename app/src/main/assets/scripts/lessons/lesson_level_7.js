
var cars = ['Honda', 'Toyota', 'Mazda'];

// Access the value of the first element in cars
var name = cars[0];
console.log(console.log('car name: ' + name));

// Modify the second element in cars
cars[1] = 'Mitsubishi';
console.log('car name: ' + cars[1]);