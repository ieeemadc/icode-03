/* Arithmetic Operations */

var a = 10;
var b = 50;
var c = 25;
var x;

//Adding
x = a + b + c;
console.log('Adding: '  + x);

// Subtracting
x = a - b - c;
console.log('Subtracting: ' + x);

// Multiplying
x = a * b * c;
console.log('Multiplying: ' + x);

// Dividing
x = a / b;
console.log('Dividing: ' + x);

// Modulus
x = a % b;
console.log('Modulus: ' + x);

// Incrementing
x = 4;
x++;
console.log('Incrementing: ' + x);

// Decrementing
x = 4;
x--;
console.log('Decrementing: ' + x);