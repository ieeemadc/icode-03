
// Calling the showMessage() function
console.log('Message: ' + showMessage());

// Function without parameters
function showMessage() {
    return 'Hello';
}

// Function is called
var x = myFunction(4, 3);
console.log('Product: '+ x);

// Function returns the product of a and b
function myFunction(a, b) {
    return a * b;
}