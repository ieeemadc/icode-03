// Variable with value 3
var a = 3;
console.log('a: ' + a);

// Variable with out defined value
var b;

// Optional, without putting [var] but with defined value
c = 4;
console.log('c: ' + c);

// Variable with a text string
var d = 'I am learning JavaScript';
console.log('d: ' + d);

// Boolean variable
var e = true;
console.log('e: ' + e);